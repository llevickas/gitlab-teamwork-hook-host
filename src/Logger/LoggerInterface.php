<?php

namespace Logger;

interface LoggerInterface
{
    public function ensureLogPath(): void;

    public function logAction($action): void;

    public function logEvent($content): void;

    public function setLogPath($logPath);
}
