<?php

namespace Logger;

/**
 * Class FileLogger.
 *
 * @author Lukas Levickas @ Synergy Effect
 */
class FileLogger implements LoggerInterface
{
    const ACTION_FILE = 'event.log';

    /**
     * @var string
     */
    private $logPath;

    public function setLogPath($logPath)
    {
        $this->logPath = $logPath;
        $this->ensureLogPath($this->logPath);
    }

    public function ensureLogPath(): void
    {
        if (!file_exists($this->logPath)) {
            mkdir($this->logPath, 0777, true);
        }
    }

    public function logEvent($content): void
    {
        $this->ensureLogPath();

        file_put_contents($this->logPath.'/'.strval(time()).'.json', is_string($content) ? $content : json_encode($content));
    }

    public function logAction($action): void
    {
        $this->ensureLogPath();

        file_put_contents(
            $this->logPath.'/'.self::ACTION_FILE,
            sprintf("[%s] %s\n", (new \DateTime())->format('Y-m-d H:i:s'), trim($action)),
            FILE_APPEND
        );
    }
}
