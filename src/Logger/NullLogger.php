<?php

namespace Logger;

/**
 * Class NullLogger.
 *
 * @author Lukas Levickas @ Synergy Effect
 */
class NullLogger implements LoggerInterface
{
    public function ensureLogPath(): void
    {
    }

    public function logAction($action): void
    {
    }

    public function logEvent($content): void
    {
    }

    public function setLogPath($logPath)
    {
    }
}
