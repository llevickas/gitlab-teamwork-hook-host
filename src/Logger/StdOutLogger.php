<?php

namespace Logger;

/**
 * Class StdOutLogger.
 *
 * @author Lukas Levickas @ Synergy Effect
 */
class StdOutLogger implements LoggerInterface
{
    public function ensureLogPath(): void
    {
        ini_set('log_errors', 1);
        ini_set('error_reporting', E_ALL);
    }

    public function logAction($action): void
    {
        error_log(trim($action));
    }

    public function logEvent($content): void
    {
        error_log(is_string($content) ? $content : json_encode($content));
    }

    public function setLogPath($logPath)
    {
    }
}
