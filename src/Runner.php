<?php

use Logger\LoggerInterface;

class Runner
{
    const CONFIG_FILE = 'config.json';
    const EVENT_PRE_MESSAGE = "%s has triggered **\"%s\"** event:\n\n";
    const EVENT_POST_MESSAGE = "\n\n---";
    const ERROR_ROOT_DIR = 'the root directory: %s is not actually correct. It must contain composer\'s vendor folder.';
    const TW_URI_TASKS = '/#/tasks';

    /**
     * @var array
     */
    private $config;

    private LoggerInterface $logger;

    private string $rootDir;

    public function __construct(string $rootDir)
    {
        $this->rootDir = realpath($rootDir);

        if (!file_exists($this->rootDir.'/vendor/autoload.php')) {
            throw new \Exception(sprintf(self::ERROR_ROOT_DIR, $this->rootDir));
        }

        try {
            $logPath = $this->getConfig('logPath');
            $this->logger = new \Logger\FileLogger();
            $this->logger->setLogPath($logPath);
            ini_set('log_errors', 1);
            ini_set('error_log', $logPath.'/errors.log');
        } catch (Exception $e) {
            $this->logger = new \Logger\StdOutLogger();
        }
    }

    public function getConfig(string $option)
    {
        if (empty($this->config)) {
            $file = $this->rootDir.'/'.self::CONFIG_FILE;

            if (!file_exists($this->rootDir)) {
                mkdir($this->rootDir, 0777, true);
            }

            if (!file_exists($file)) {
                file_put_contents($file, json_encode([
                    'logPath' => 'logs',
                    'projectIncomingHooks' => [
                        'gitlabprojectname' => 'https://subdomain.teamwork.com/teamwork/specific/project/chat/incoming/hook/url',
                        '_global' => 'https://subdomain.teamwork.com/teamwork/chat/incoming/hook/url/for/anything/unspecified',
                    ],
                    'secret' => 'gitlab-webhooks-secret',
                    'teamworkHost' => 'https://subdomain.teamwork.com',
                ], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));

                $message = sprintf('No configuration file found, so created one here: %s. Fill in the values!', $file);

                throw new \Exception($message);
            }

            $config = file_get_contents($file);

            if (false === $config) {
                throw new \Exception(sprintf('Could not read the config file: %s.', $file));
            }

            $config = json_decode($config, true);

            if (!is_array($config)) {
                throw new \Exception(sprintf('Failed parsing config file as a JSON, file: %s.', $file));
            }

            $this->config = $config;
        }

        if (empty($this->config[$option])) {
            throw new \Exception(sprintf('Config has no option named "%s"', $option));
        }

        return $this->config[$option];
    }

    public function formatPushChatMessage($content): string
    {
        if (empty($content['commits'])) {
            $this->logger->logAction('push event detected, but expected commits property was not found.');
        }

        $message = sprintf("[Repository](%s) commits:\n\n", $content['repository']['git_http_url'], PHP_EOL);

        foreach ($content['commits'] as $commit) {
            $taskUrl = $this->getTaskUrlFromMessage($commit['message']);

            if ($taskUrl) {
                $message .= sprintf(
                    "* [Commit](%s) at %s by **%s**:\n\n    * [Teamwork Task](%s) %s\n\n",
                    $commit['url'],
                    $commit['timestamp'],
                    $commit['author']['name'],
                    $taskUrl,
                    $commit['message'],
                );
            } else {
                $message .= sprintf(
                    "* [Commit](%s) at %s by **%s**:\n\n    * %s\n\n",
                    $commit['url'],
                    $commit['timestamp'],
                    $commit['author']['name'],
                    $commit['message'],
                );
            }
        }

        return $message;
    }

    public function formatMergeRequestChatMessage($content): string
    {
        if (empty($content['object_attributes'])) {
            $this->logger->logAction('Merge request event detected, but expected object_attributes property was not found.');
        }

        $message = sprintf(
            "[Repository](%s) has new [merge request %s -- %s: %s](%s) at %s by **%s** \n\n",
            $content['repository']['homepage'],
            $content['object_attributes']['source_branch'],
            $content['object_attributes']['target_branch'],
            $content['object_attributes']['title'],
            $content['object_attributes']['url'],
            $content['object_attributes']['created_at'],
            $content['user']['name'],
            PHP_EOL
        );

        return $message;
    }

    public function formatPipelineChatMessage($content): string
    {
        if (empty($content['object_attributes'])) {
            $this->logger->logAction('Merge request event detected, but expected object_attributes property was not found.');
        }

        $message = sprintf(
        "Pipeline started at %s and finished at %s by **%s** with status: **%s**\n\n",
        $content['object_attributes']['created_at'],
        $content['object_attributes']['finished_at'],
        $content['user']['name'],
        $content['object_attributes']['status'],
        PHP_EOL
    );

        return $message;
    }

    public function postEventChatMessage($content): bool
    {
        $projectIncomingHooks = $this->getConfig('projectIncomingHooks');

        if (empty($content['object_kind'])) {
            return false;
        }

        if (empty($content['project']['name'])) {
            return false;
        }

        $url = '';
        $projectName = mb_strtolower($content['project']['name']);

        if (array_key_exists($projectName, $projectIncomingHooks)) {
            $url = $projectIncomingHooks[$projectName];
        } elseif (!empty($projectIncomingHooks['_global'])) {
            $url = $projectIncomingHooks['_global'];
        } else {
            return false;
        }

        $person = empty($content['user_name']) ? 'Somebody' : $content['user_name'];
        $event = $content['object_kind'];
        $message = '';
        $ch = curl_init($url);

        switch ($event) {
        case 'push':
            $message = $this->formatPushChatMessage($content);
            break;
        case 'merge_request':
            $message = $this->formatMergeRequestChatMessage($content);
            break;
        case 'pipeline':
            $message = $this->formatPipelineChatMessage($content);
            break;
        }

        $message = sprintf(self::EVENT_PRE_MESSAGE, $person, $event).$message.self::EVENT_POST_MESSAGE;

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['body' => $message]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    public function run(): bool
    {
        $input = file_get_contents('php://input');
        $stdin = file_get_contents('php://stdin');
        $secret = $this->getConfig('secret');
        $content = json_decode(empty($input) ? $stdin : $input, true);

        if (!empty($input) && empty($_SERVER['HTTP_X_GITLAB_TOKEN'])) {
            $this->logger->logAction('Received incoming event without a secret token. Ignoring.');

            return false;
        } elseif (!empty($input) && $_SERVER['HTTP_X_GITLAB_TOKEN'] !== $secret) {
            $this->logger->logAction('Received incoming event with wrong secret token. Ignoring.');

            return false;
        } elseif (empty($content)) {
            $this->logger->logAction('Initiated with empty content.');

            return false;
        }

        $this->logger->logAction('Incoming request.');
        $this->logger->logEvent($content);

        if (!empty($input)) {
            $this->postEventChatMessage($content);
        }

        return true;
    }

    public function logAction($action)
    {
        return $this->logger->logAction($action);
    }

    public function logEvent($content)
    {
        return $this->logger->logEvent($content);
    }

    /**
     * Get Teamwork Task URL from string.
     *
     * @param string $message the message to extract Teamwork Task URL from
     */
    public function getTaskUrlFromMessage(string $message): ?string
    {
        $urlPart = $this->getConfig('teamworkHost').self::TW_URI_TASKS;

        preg_match('#(?>^|\s|\-|\()(?<taskid>[1-9][0-9]*)(?>\s|\-|\.|\)|$)#i', $message, $matches);

        if (empty($matches['taskid'])) {
            return null;
        }

        return sprintf('%s/%s', $urlPart, $matches['taskid']);
    }
}
