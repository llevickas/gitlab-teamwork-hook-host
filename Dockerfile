FROM webdevops/php-nginx:7.4-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1 
ENV WEB_DOCUMENT_INDEX=public/index.php

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ADD ./ /app

RUN cd /app && composer install \
		--no-dev \
		--prefer-dist \
		--no-scripts \
		--no-progress \
		--no-suggest \
		--no-interaction \
		--optimize-autoloader
