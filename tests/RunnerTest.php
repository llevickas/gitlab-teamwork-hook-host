<?php

declare(strict_types=1);

require_once realpath(__DIR__.'/..').'/vendor/autoload.php';

use PHPUnit\Framework\TestCase;

final class RunnerTest extends TestCase
{
    private $rootDir;
    private $runner;
    private $logPath;

    protected function setUp(): void
    {
        $this->rootDir = realpath(__DIR__.'/..');
        $this->runner = new Runner($this->rootDir);

        try {
            $this->logPath = $this->runner->getConfig('logPath');
        } catch (Exception $e) {
            $this->logPath = null;
        }
    }

    public function testGetConifg()
    {
        if (null !== $this->logPath) {
            $this->assertDirectoryExists($this->runner->getConfig('logPath'));
        }

        $this->assertIsArray($this->runner->getConfig('projectIncomingHooks'));
        $this->assertIsString($this->runner->getConfig('secret'));
        $this->assertIsString($this->runner->getConfig('teamworkHost'));
    }

    public function testEnsureLogPath()
    {
        if (null !== $this->logPath) {
            $this->assertDirectoryExists($this->runner->getConfig('logPath'));
            $this->assertDirectoryExists($this->logPath);
            $this->assertDirectoryIsWritable($this->logPath);
        } else {
            $this->assertTrue(true);
        }
    }

    public function testLogEvent()
    {
        if (null !== $this->logPath) {
            $was = iterator_count((new FilesystemIterator($this->logPath, FilesystemIterator::SKIP_DOTS)));

            $this->runner->logEvent(['key' => 'value']);

            $is = iterator_count((new FilesystemIterator($this->logPath, FilesystemIterator::SKIP_DOTS)));

            $this->assertGreaterThan($was, $is);
        } else {
            $this->assertTrue(true);
        }
    }

    public function testLogAction()
    {
        if (null !== $this->logPath) {
            $string = 'testLogAction';
            $file = $this->logPath.'/'.\Logger\FileLogger::ACTION_FILE;
            @unlink($file);
            $this->runner->logAction($string);
            $this->assertFileExists($file);
        } else {
            $this->assertTrue(true);
        }
    }

    public function testFormatPushChatMessage()
    {
        $examples = [
            [
                'commitMessage' => 'Lorem Ipsum',
                'commitUrl' => 'http://commit.url',
                'name' => 'John Doe',
                'repoUrl' => 'http://example.com/acme/code.git',
                'timestamp' => (new \DateTime())->getTimestamp(),
            ],
            [
                'taskID' => '123123',
                'commitMessage' => 'Fixed tasks 123123 bug.',
                'commitUrl' => 'http://commit.url',
                'name' => 'John Doe',
                'repoUrl' => 'http://example.com/acme/code.git',
                'timestamp' => (new \DateTime())->getTimestamp(),
            ],
        ];

        foreach ($examples as $example) {
            $message = $this->runner->formatPushChatMessage([
                'repository' => [
                    'git_http_url' => $example['repoUrl'],
                ],
                'commits' => [
                    [
                        'author' => [
                            'name' => $example['name'],
                        ],
                        'message' => $example['commitMessage'],
                        'timestamp' => $example['timestamp'],
                        'url' => $example['commitUrl'],
                    ],
                ],
            ]);

            $this->assertMatchesRegularExpression('#'.$example['commitMessage'].'#', $message);
            $this->assertMatchesRegularExpression('#'.$example['commitUrl'].'#', $message);
            $this->assertMatchesRegularExpression('#'.$example['name'].'#', $message);
            $this->assertMatchesRegularExpression('#'.$example['repoUrl'].'#', $message);
            $this->assertMatchesRegularExpression('#'.$example['timestamp'].'#', $message);

            if (!empty($example['taskID'])) {
                $this->assertMatchesRegularExpression(
                    '#\[Teamwork Task\]\('
                        .str_replace('/', '\/', $this->runner->getConfig('teamworkHost'))
                        .str_replace('/', '\/', str_replace('#', '\#', $this->runner::TW_URI_TASKS))
                        .'\/'
                        .$example['taskID']
                        .'\)#i',
                    $message
                );
            }
        }
    }

    public function testFormatMergeRequestChatMessage()
    {
        $createdAt = (new \DateTime())->getTimestamp();
        $mergeTitle = 'The merge';
        $mergeUrl = 'http://commit.url';
        $name = 'John Doe';
        $repoUrl = 'http://example.com/acme/code.git';
        $sourceBranch = 'source';
        $targetBranch = 'target';

        $message = $this->runner->formatMergeRequestChatMessage([
            'object_attributes' => [
                'source_branch' => $sourceBranch,
                'target_branch' => $targetBranch,
                'title' => $mergeTitle,
                'url' => $mergeUrl,
                'created_at' => $createdAt,
            ],
            'repository' => [
                'homepage' => $repoUrl,
            ],
            'user' => [
                'name' => $name,
            ],
        ]);

        $this->assertMatchesRegularExpression('#'.$createdAt.'#', $message);
        $this->assertMatchesRegularExpression('#'.$mergeTitle.'#', $message);
        $this->assertMatchesRegularExpression('#'.$mergeUrl.'#', $message);
        $this->assertMatchesRegularExpression('#'.$name.'#', $message);
        $this->assertMatchesRegularExpression('#'.$repoUrl.'#', $message);
        $this->assertMatchesRegularExpression('#'.$sourceBranch.'#', $message);
        $this->assertMatchesRegularExpression('#'.$targetBranch.'#', $message);
    }

    public function testFormatPipelineChatMessage()
    {
        $createdAt = (new \DateTime())->getTimestamp();
        $finishedAt = (new \DateTime('+30 minutes'))->getTimestamp();
        $name = 'John Doe';
        $status = 'lit';

        $message = $this->runner->formatPipelineChatMessage([
            'object_attributes' => [
                'created_at' => $createdAt,
                'finished_at' => $finishedAt,
                'status' => $status,
            ],
            'user' => [
                'name' => $name,
            ],
        ]);

        $this->assertMatchesRegularExpression('#'.$createdAt.'#', $message);
        $this->assertMatchesRegularExpression('#'.$finishedAt.'#', $message);
        $this->assertMatchesRegularExpression('#'.$name.'#', $message);
        $this->assertMatchesRegularExpression('#'.$status.'#', $message);
    }

    public function testPostEventChatMessage()
    {
        $this->assertFalse($this->runner->postEventChatMessage([
            'object_kind' => 'push',
            'project' => [
                'name' => 'codebase',
            ],
            'repository' => [
                'git_http_url' => 'http://example.com/acme/code.git',
            ],
            'commits' => [
                [
                    'author' => [
                        'name' => 'John Doe',
                    ],
                    'message' => 'Lorem Ipsum',
                    'timestamp' => (new \DateTime())->getTimestamp(),
                    'url' => 'http://commit.url',
                ],
            ],
        ]));
    }

    public function testGetTaskUrlFromMessage()
    {
        $urlStart = $this->runner->getConfig('teamworkHost').$this->runner::TW_URI_TASKS.'/';
        $variations = [
            [
                'message' => '12304123 Fixed homepage.',
                'url' => $urlStart.'12304123',
            ],
            [
                'message' => 'Commit 1ueou123 fix.',
                'url' => null,
            ],
            [
                'message' => '0 Issues found.',
                'url' => null,
            ],
            [
                'message' => '1 Task Function added.',
                'url' => $urlStart.'1',
            ],
            [
                'message' => 'Merge feature-123132-omg branch to master.',
                'url' => $urlStart.'123132',
            ],
            [
                'message' => 'Closed task 123444213.',
                'url' => $urlStart.'123444213',
            ],
            [
                'message' => 'addresses (123444213)',
                'url' => $urlStart.'123444213',
            ],
            [
                'message' => 'Closed task 00000000.',
                'url' => null,
            ],
        ];

        foreach ($variations as $variation) {
            $this->assertEquals($variation['url'], $this->runner->getTaskUrlFromMessage($variation['message']));
        }
    }
}
