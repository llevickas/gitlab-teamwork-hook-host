# Readme

*The [Gitlab Webhook](https://gitlab.com/help/user/project/integrations/webhooks) integration into [teamwork](https://www.teamwork.com/)*

## Setup

* Edit the config.json so that:
  * `secret` matches the one in the webhooks settings.
  * `projectIncomingHooks` contains the '[LOWERCASE_GITLAB_PROJECT_NAME]: [TEAMWORK_CHAT_INCOMING_HOOK_URL]'

If you want to get all webhooks into a global incoming hook url regardles of project,
use `.projectIncomingHooks._global` property.

**Ensure the php has the rights to write into logs path!**
